
# KyDev

This is the repository for my personal portfolio, where I showcase my projects and skills. The site is designed to provide visitors with a clear overview of my career and competencies.

## Features

Homepage: Introduction and a brief summary about me.

Projects: Detailed section featuring my most prominent projects, including descriptions, images, and links.

Education: Overview of my educational background, including dates and certifications.

Contact: A form that allows visitors to easily get in touch with me.



## Installation

1. Clone the repository:

```bash
  git clone https://gitlab.com/criistiiniilla.1994/kydev.git
cd kydev

```
    

2. Install the dependencies:

```bash
  npm install
  yarn install
```

3. Start the development server:

```bash
  npm run dev
  yarn run dev
```
## Contacto

If you have any questions or comments, feel free to reach out to me at labradorcristina8@gmail.com.



[Demo](https://kydev.vercel.app/)


