'use client'
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import Image from "next/image";


export default function HomePage() {
  return (
    <div className="pt-48 xs:h-[350vh] relative text-black">
      <div className='mb-20'>

        <Image className="absolute top-[280px] left-1/2 transform -translate-x-1/2 rounded-md" src="/me.jpeg" width={300} height={300} alt="Cris Labrador" />
        
        <h1 className="my-5 text-3xl text-center uppercase text-white ">Cris Labrador</h1>
        <section className="flex flex-col items-center justify-center h-auto px-4 bg-gray-300 rounded-md mx-6 md:mx-20 lg:mx-40 xl:mx-60 mt-32 mb-40">
          <div className=' mt-64 mb-4 hover:scale-105'>
          <a
              href="/files/KyDev_CV.pdf"
              download="KyDev_CV"
              className="border rounded-md inline-flex items-center px-6 py-4 bg-black text-white"
            >
              <img
                src="/download-1.png"
                alt="Download icon"
                className="w-5 h-5 mr-4"
              />
              Download CV
            </a>
          </div>
          <section className="flex flex-col justify-center text-center">
            <p className="text-center text-xl font-sans">FullStack Developer JavaScript</p>
            <p className="py-10 text-center text-lg leading-loose max-w-2xl">
              Web developer specialized in JavaScript technologies. A few months ago, I completed my training at the HackABoss bootcamp with a specialization in web development and I'm excited to start my professional career creating intuitive and functional web applications.
            </p>
          </section>

          <p className="text-center text-lg leading-loose max-w-2xl">
            During my training and through personal projects, I have gained experience working with:
          </p>
          <ul className="flex flex-wrap justify-center mt-10 m-2 text-center text-black text-sm  w-90">
            <li className="m-4">
              <p>JavaScript</p>
              <Image className="m-4" src="/js.svg" width={40} height={40} alt="JavaScript" />
            </li>
            <li className="m-4">
              <p>Python</p>
              <Image className="m-4" src="/python.svg" width={40} height={40} alt="MYSQL" />
            </li>
            <li className="m-4">
              <p>TypeScript</p>
              <Image className="m-4" src="/typescript-color.svg" width={40} height={40} alt="Typescript" />
            </li>
            <li className="m-4">
              <p>React</p>
              <Image className="m-4" src="/react.svg" width={40} height={40} alt="React" />
            </li>
            <li className="m-4">
              <p>Nextjs</p>
              <Image className="m-4" src="/NextJS.svg" width={40} height={40} />
            </li>
            <li className="m-4">
              <p>Tailwind</p>
              <Image className="m-4" src="/Tailwind.svg" width={40} height={40} />
            </li>
            <li className="m-4">
              <p>Bootsrtap</p>
              <Image className="m-4" src="/bootstrap.svg" width={40} height={40} />
            </li>
            <li className="m-4">
              <p>MaterialUI</p>
              <Image className="m-4" src="/materialui.svg" width={40} height={40} alt="MYSQL" />
            </li>
            <li className="m-4">
              <p>Node.js</p>
              <Image className="m-4" src="/nodejs.svg" width={40} height={40} alt="Node.js" />
            </li>
            <li className="m-4">
              <p>MYSQL</p>
              <Image className="m-4" src="/MySQL.svg" width={40} height={40} alt="MYSQL" />
            </li>
            <li className="m-4">
              <p>MongoDB</p>
              <Image className="mt-4 ml-6" src="/mongodb.svg" width={20} height={20} alt="MYSQL" />
            </li>
            <li className="m-4">
              <p>Socket.io</p>
              <Image className="m-4" src="/socket.io.svg" width={40} height={40} alt="MYSQL" />
            </li>
          </ul>
        </section>

      </div>
    </div>
  );
}
