
import Header from "./components/Header";
import { Montserrat } from "next/font/google";
import "./globals.css";

const roboto = Montserrat({
  weight: ["300", "400", "500", "700"],
  styles: ["italic", "normal"],
  subsets: ["latin"],
})


export const metadata = {
  title: "KyDev",
  description: "portfolio KyDev",
  icons: {
    icon: '/logo-kydev.png', // ruta a tu favicon en la carpeta public
  },
};

export default function RootLayout({ children }) {

  return (

    <html lang="en">
      <link rel="icon" href="favicon.ico" type="image/x-icon" />
      <body className={roboto.className}>
        <Header />
        <main className="mt-22">
          {children}
        </main>
      </body>
    </html>

  );
}
