import Image from "next/image"

function Certifications() {
  return (
    <div className="certifications z-0">
      <ul className="flex flex-col justify-center items-center gap-5 p-20">
        <li >
          <Image className="rounded" src={"/hab.png"} width={800} height={100}/>
        </li>
        <li >
          <Image className="rounded" src={"/node.jpg"} width={800} height={100} />
        </li>
        <li>
          <Image className="rounded" src={"/js.jpg"} width={800} height={100} />
        </li>
      </ul>
    </div>
  )
}

export default Certifications