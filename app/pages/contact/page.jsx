'use client'

import { useState } from 'react';
import emailjs from 'emailjs-com';
import { ToastContainer, toast, Flip } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Contact = () => {
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    message: ''
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const result = await emailjs.send(
        process.env.NEXT_PUBLIC_EMAILJS_SERVICE_ID,
        process.env.NEXT_PUBLIC_EMAILJS_TEMPLATE_ID,
        formData,
        process.env.NEXT_PUBLIC_EMAILJS_USER_ID
      );
      console.log('Message sent: ', result.text);
      toast('Email enviado con éxito', {
        autoClose: 1000,
        position: 'bottom-center',
        theme: 'light',
        transition: Flip,
      });
      setFormData({ name: '', email: '', message: '' });
    } catch (error) {
      console.error('Error sending message: ', error);
      toast.error('Failed to send message.');
    }
  };

  return (
    <div className="flex flex-col items-center mb-20 p-5 sm:p-10 w-full z-0">
      <h2 className="text-3xl sm:text-4xl uppercase text-center mb-10 mt-28"> Contact Me!</h2>
      <form className="rounded-lg flex flex-col px-5 py-5 w-full sm:w-5/12 gap-3 border  border-white bg-gray-700" onSubmit={handleSubmit}>
        <label className='font-bold text-lg sm:text-xl text-teal-600' htmlFor="name">Name</label>
        <input
          className="bg-transparent border rounded-md py-2 p-2 focus:outline-none font-light"
          type="text"
          id="name"
          name="name"
          value={formData.name}
          onChange={handleChange}
          required
        />
        <label className='font-bold text-lg sm:text-xl text-teal-600' htmlFor="email">Email</label>
        <input
          className="bg-transparent border rounded-md py-2 p-2 focus:outline-none focus:rounded-md font-light"
          type="email"
          id="email"
          name="email"
          value={formData.email}
          onChange={handleChange}
          required
        />
        <label className='font-bold text-lg sm:text-xl text-teal-600' htmlFor="message">Message</label>
        <textarea
          className="bg-transparent border rounded-md py-2 p-2 focus:outline-none focus:rounded-md font-light resize-none"
          id="message"
          name="message"
          value={formData.message}
          onChange={handleChange}
          required
        />
        <div className='flex justify-center w-full p-2'>
          <button className="font-bold  w-52 sm:text-lg mt-8 py-2 uppercase text-xl bg-teal-800  rounded hover:bg-teal-700 " type="submit">Send Email</button>
        </div>
      </form>
      <ToastContainer/>
    </div>

  );
};

export default Contact;
