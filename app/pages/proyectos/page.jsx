'use client'
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Pagination } from 'swiper/modules';
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import Image from "next/image";
import Link from "next/link";

export default function Proyectos() {
  return (
    <div id='projects' className="w-full p-5">
      <h1 className='text-white flex justify-center uppercase mb-10 text-4xl mt-24'>Projects</h1>
      <Swiper
        modules={[Navigation, Pagination]}
        navigation
        pagination={{ clickable: true }}
        spaceBetween={50}
        slidesPerView={1}
      >
        <SwiperSlide>
          <div className=" rounded-2xl h-auto flex flex-col justify-center items-center p-16 md:m-16">
            <h2 className="text-xl md:text-2xl uppercase p-5 font-bold">FilmGeek</h2>
            <div className="flex justify-center text-center">
              <Image className="mb-5 border rounded-md" src="/filmgeek.png" width={500} height={500} alt="FilmGeek" />
            </div>
            <p className="text-lg md:text-xl text-center">FilmGeek is a app that allows you to explore, discover and enjoy a wide variety of movies from different genres. Looking for the latest movie releases? FilmGeek has something for every movie lover, hope you like it! 🚀</p>
            <p className="text-lg md:text-xl text-center">React for the frontend and css, I use an external api
              called The Movie Database.
              This project is uploaded in GitLab</p>
            <Link className="uppercase text-2xl bg-gray-600 py-2 px-2 hover:bg-gray-500 rounded inline-block mt-4" href="https://gitlab.com/criistiiniilla.1994/film-geek" target="_blank">Repository</Link>
            <Link className="uppercase text-2xl bg-gray-600 py-2 px-2  hover:bg-gray-500 rounded inline-block mt-4 ml-4" href="https://film-geek.vercel.app/" target="_blank">Demo</Link>
          </div>
        </SwiperSlide>

        <SwiperSlide>
          <div className=" rounded-2xl h-auto flex flex-col justify-center items-center p-12 md:m-16">
            <h2 className="text-xl md:text-4xl uppercase p-5 font-bold">Wanderlust</h2>
            <div className="flex justify-center text-center">
              <Image className="mb-5 mt-5 border rounded-md" src="/wanderlust.png" width={600} height={500} alt="Wanderlust" />
            </div>
            <p className="text-lg md:text-xl text-center">Wanderlust is a social network conceived and designed for exploration enthusiasts to share their trips, excursions, experiences, getaways, tricks and hidden corners. Whether you are a veteran globetrotter or you are looking for inspiration for your next trip, plan, excursion, etc. Genuine suggestions from our travelers await you on Wanderlust.</p>
            <p className="text-lg md:text-xl text-center">I use React for the frontend and Nodejs for the
              backend along with Express for interaction with the
              SQL relational database.</p>
            <Link className="uppercase text-2xl rounded inline-block mt-4 bg-gray-600 py-2 px-2 hover:bg-gray-500" href="https://github.com/crisky94/ProyectoBlogViajes" target="_blank">Repository</Link>
          </div>
        </SwiperSlide>

        <SwiperSlide>
          <div className=" rounded-2xl h-auto flex flex-col justify-center items-center p-12 md:m-16">
            <h2 className="text-xl md:text-4xl uppercase p-5 font-bold">Freedayquiz</h2>
            <div className="flex justify-center text-center">
              <Image className="mb-5 mt-5 border rounded-md" src="/freedayquiz.png" width={600} height={500} alt="Wanderlust" />
            </div>
            <p className="text-lg md:text-xl text-center">This app is for create and play quizzes in virtual live. This game management system is ideal for creating interactive question and answer experiences, providing customization options for both administrators and players. Each profile is designed to facilitate the flow of the game, ensuring a dynamic and entertaining experience.</p>
            <p className="text-lg md:text-xl text-center">In this project we use Nextjs and tailwind for the css. As
              it is an app that needs real time functionalities, we
              implemented websocket on the server along with
              prisma for the queries to the relational database.
              For the layout of the app we used figma.
              And for the organization of the team we use the agile
              scrum methodology.</p>
            <Link className="uppercase text-2xl rounded inline-block mt-4 bg-gray-600 py-2 px-2 hover:bg-gray-500" href="https://github.com/crisky94/Freeday-quiz" target="_blank">Repository</Link>
            <Link className="uppercase text-2xl bg-gray-600 py-2 px-2  hover:bg-gray-500 rounded inline-block mt-4 ml-4" href="https://freeday-quiz.onrender.com/" target="_blank">Demo</Link>
          </div>
        </SwiperSlide>

        <SwiperSlide>
          <div className=" rounded-2xl h-auto  max-h-[750px] flex flex-col justify-center items-center p-15 md:m-16">
            <h2 className="text-xl md:text-4xl uppercase p-5 font-bold">ToDoList</h2>
            <div className="flex justify-center text-center">
              <Image className="mb-5 mt-5 border rounded-md" src="/todolist.png" width={600} height={500} alt="Wanderlust" />
            </div>
            <p className="text-lg md:text-xl text-center">It is a project to put a list of tasks, to be able to cross out the task done and delete as you like.</p>
            <p className="text-lg md:text-xl text-center">This project is made with typescript and React. For the saving of the list, I use the localstorage.</p>
            <Link className="uppercase text-2xl rounded inline-block mt-4 bg-gray-600 py-2 px-2 hover:bg-gray-500" href="https://github.com/crisky94/todoList" target="_blank">Repository</Link>
            <Link className="uppercase text-2xl bg-gray-600 py-2 px-2  hover:bg-gray-500 rounded inline-block mt-4 ml-4" href="https://todo-list-five-inky.vercel.app/" target="_blank">Demo</Link>
          </div>
        </SwiperSlide>
      </Swiper>
    </div>
  )
}
