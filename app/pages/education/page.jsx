'use client'; // Asegúrate de que esta línea esté en la parte superior

import Link from "next/link";
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Pagination } from 'swiper/modules';
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';

export default function Education() {
  return (
    <div className="text-center min-h-screen w-full -z-10">
      <Link
        href="/pages/certifications"
        className="uppercase text-2xl bg-teal-800  rounded hover:bg-teal-700 p-2 inline-block mt-28"
      >
        Certificates
      </Link>

      <Swiper
        modules={[Navigation, Pagination]} // Asegúrate de importar correctamente los módulos
        navigation // Activa la navegación
        pagination={{ clickable: true }} // Activa la paginación
        spaceBetween={50}
        slidesPerView={1}
      >
        <SwiperSlide>
          <div className="mt-2 p-2 md:p-28 z-0">
            <div className="text-2xl mb-2"><i>2023-2024</i></div>
            <h3 className="text-4xl mb-2">Bootcamp - Hack A Boss</h3>
            <p className="text-xl text-center leading-loose">
              - Knowledge and skills in HTML, CSS, JavaScript, Node.js, React.js, MySQL, GIT, GITLAB, GITHUB.<br />
              - Creation of responsive web applications, implementation of APIs and databases.
              - Practical and collaborative projects through which I gained experience in the use of version control tools (GIT).<br />
              - Basic understanding of agile methodologies and the ability to work effectively in development teams.
            </p>
          </div>
        </SwiperSlide>

        <SwiperSlide>
          <div className="mt-2 p-2 md:p-20">
            <div className="text-2xl mb-2"><i>DIC 2023</i></div>
            <h3 className="text-4xl mb-2">JavaScript ES9, HTML, CSS y NodeJS desde cero - Udemy</h3>
            <p className="text-xl text-start leading-loose p-12">
              <span className="text-center block">Solidar los conceptos aprendidos en la bootcamp:</span>
              - New features introduced in JavaScript ES9, such as propagation operators, object destructuring and asynchronous functions.<br/>
              - Practicing modern JavaScript syntax to write cleaner and more efficient code.<br />
              - Sobre Node.js, un entorno de tiempo de ejecución de JavaScript que te permite ejecutar código JavaScript en el servidor. Explora cómo instalar Node.js, crear y ejecutar archivos JavaScript en el servidor, y utilizar módulos npm (Node Package Manager).
            </p>
          </div>
        </SwiperSlide>

        <SwiperSlide>
          <div className="mt-2 p-2 md:p-20">
            <div className="text-2xl mb-2"><i>DIC 2023</i></div>
            <h3 className="text-4xl mb-2">Introduction to Programming Using JavaScript - Udemy</h3>
            <p className="text-xl leading-loose p-12">
              Solidify the basic concepts of JavaScript from the bootcamp, such as variables, data types, operators, functions, control structures and objects.
            </p>
          </div>
        </SwiperSlide>
      </Swiper>
    </div>
  );
}
