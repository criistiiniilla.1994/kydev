'use client'
import Link from "next/link";
import Image from "next/image";
import { useState } from "react";

export default function Header() {
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  // Función para manejar la apertura/cierre del menú
  const toggleMenu = () => {
    setIsMenuOpen(!isMenuOpen);
  };

  // Función para cerrar el menú cuando se haga clic en un enlace
  const closeMenu = () => {
    setIsMenuOpen(false);
  };

  return (
    <nav className="bg-gray-700 fixed w-full z-10">
        <div className="relative flex h-16 items-center justify-between m-4">
          <div className="absolute inset-y-0 right-0 flex items-center sm:hidden">
          <button
            onClick={toggleMenu} // Activa el estado del menú
            type="button"
            className="relative inline-flex items-center justify-center rounded-md p-2 text-gray-400 hover:bg-gray-600 hover:text-white focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white"
            aria-controls="mobile-menu"
            aria-expanded={isMenuOpen}
          >
            <span className="sr-only">Open main menu</span>
            <svg
              className={`${isMenuOpen ? 'hidden' : 'block'} h-6 w-6`}
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth="1.5"
              stroke="currentColor"
              aria-hidden="true"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"
              />
            </svg>
            <svg
              className={`${isMenuOpen ? 'block' : 'hidden'} h-6 w-6`}
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth="1.5"
              stroke="currentColor"
              aria-hidden="true"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M6 18L18 6M6 6l12 12"
              />
            </svg>
          </button>
          </div>
          <div className="flex flex-shrink-0 items-center">

            <Link className="w-full " href="/">
              <Image src={"/logo-kydev.png"} width={50} height={50} />
            </Link>
            <Link className="w-full" href="/">
              <p className="text-teal-600 shadow-black mt-2">ydev</p>
            </Link>
          </div>
          <div className="flex flex-1 items-center justify-center sm:items-stretch sm:justify-end">
          
            <div className="hidden sm:ml-6 sm:block mt-2">
              <div className="flex space-x-4 uppercase">
                <Link href="/" className="rounded-md bg-gray-900 px-3 py-2 text-sm font-medium text-white hover:bg-slate-800">Home</Link>
                <Link href="/pages/proyectos" className="rounded-md px-3 py-2 text-sm font-medium text-gray-300 hover:text-white hover:bg-gray-600">Projects</Link>
                <Link href="/pages/education" className="rounded-md px-3 py-2 text-sm font-medium text-gray-300 hover:bg-gray-600 hover:text-white">Education</Link>
                <Link href="/pages/contact" class="rounded-md px-3 py-2 text-sm font-medium text-gray-300 hover:bg-gray-600 hover:text-white">Contact</Link>
                <Link className="px-3 hover:scale-110" href="https://github.com/crisky94?tab=repositories" target="_blank"><Image src={"/Github.svg"} width={30} height={30} /></Link>
                <Link className="px-3 hover:scale-110" href="https://gitlab.com/users/criistiiniilla.1994/projects" target="_blank"><Image src={"/gitlab.svg"} width={30} height={30} /></Link>
                <Link className="px-3 hover:scale-110" href="https://www.linkedin.com/in/cristinalabradorordonez" target="_blank"><Image src={"/linkedin.svg"} width={30} height={30} /></Link>
              </div>

            </div>
          </div>
        </div>
  
      <div className={`${isMenuOpen ? 'block' : 'hidden'} sm:hidden`} id="mobile-menu">
          <div className="space-y-1 h-auto px-2 pb-3 pt-2 uppercase flex flex-col items-center justify-center">
            <Link
              onClick={closeMenu}
              href="/pages/proyectos"
              className="block rounded-md px-3 py-2 text-sm font-medium text-white"
              aria-current="page"
            >
              Projects
            </Link>
            <Link
              onClick={closeMenu}
              href="/pages/education"
              className="block rounded-md px-3 py-2 text-sm font-medium text-gray-300 hover:bg-gray-700 hover:text-white"
            >
              Education
            </Link>
            <Link
              onClick={closeMenu}
              href="/pages/contact"
              className="block rounded-md px-3 py-2 text-sm font-medium text-gray-300 hover:bg-gray-700 hover:text-white"
            >
              Contact
            </Link>
            <div className="flex flex-row space-x-4 space-y-4">
              <Link className="mt-4" href="https://github.com/crisky94?tab=repositories" onClick={closeMenu} target="_blank"><Image src={"/Github.svg"} width={30} height={30} /></Link>
              <Link href="https://gitlab.com/dashboard/projects" onClick={closeMenu} target="_blank"><Image src={"/gitlab.svg"} width={30} height={30} /></Link>
              <Link href="https://www.linkedin.com/in/cristinalabradorordonez" onClick={closeMenu} target="_blank"><Image src={"/linkedin.svg"} width={30} height={30} /></Link>
            </div>
          </div>
        </div>
  
    </nav>

  )
}
