
function notfound() {
  return (
    <div className="flex gap-10  flex-wrap justify-center items-center text-center w-fit mt-20 min-h-screen">
      <h1 className="">Página no encontrada</h1>
    </div>
  )
}

export default notfound